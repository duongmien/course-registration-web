-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th3 16, 2022 lúc 10:52 AM
-- Phiên bản máy phục vụ: 10.4.22-MariaDB
-- Phiên bản PHP: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `course_registration`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `class`
--

CREATE TABLE `class` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `major_id` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `class`
--

INSERT INTO `class` (`id`, `name`, `major_id`, `is_deleted`) VALUES
(3, '19T1', 1, 0),
(4, '19THD1', 14, 0),
(13, '19T2', 1, 0),
(17, '19XD2', 16, 0),
(18, '20TTNT1', 17, 0),
(20, 'testg', 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `class_section`
--

CREATE TABLE `class_section` (
  `id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `name_teacher` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `classroom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `day` int(11) NOT NULL,
  `period` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `class_section`
--

INSERT INTO `class_section` (`id`, `quantity`, `name_teacher`, `classroom`, `start_date`, `day`, `period`, `semester_id`, `course_id`) VALUES
(5, 120, 'Phạm Tuấn', 'Phòng A105', '2022-03-26', 7, '1-3', 3, 5),
(6, 60, 'Nguyễn Thị Hà Quyên', 'Phòng A105', '2022-03-25', 2, '1-2', 3, 1),
(7, 60, 'Phan Gia Huy', 'Phòng A105', '2022-02-28', 2, '3', 3, 7),
(8, 60, 'Nguyễn Thị Hà Quyên', 'Phòng A105', '2022-03-26', 3, '1-2', 3, 1),
(9, 40, 'Phan Thế Hiển', 'Phòng A107', '2022-03-26', 3, '1-3', 3, 5),
(10, 70, 'Phan Thế Hiển', 'Phòng A105', '2022-03-31', 5, '1-3', 1, 1),
(11, 40, 'Thái Ngọc Sơn', 'Phòng A107', '2022-03-27', 2, '1-3', 1, 4),
(17, 60, 'Nguyễn Lan Chi', 'Phòng A108', '2022-03-25', 6, '1-3', 3, 12),
(18, 60, 'Đỗ Phú Huy', 'Phòng A106', '2022-03-13', 3, '1-4', 3, 13),
(19, 40, 'Hoàng Thị Mỹ lệ', 'Phòng B201', '2022-03-28', 5, '1-3', 3, 15),
(20, 60, 'Lê Vũ', 'Phòng B206', '2022-03-30', 2, '9-10', 3, 14),
(21, 40, 'Hoàng Huynh', 'Phòng A105', '2022-03-15', 2, '1-2', 3, 9),
(22, 40, 'Hoàng Huynh', 'Phòng A107', '2022-03-26', 3, '9-10', 3, 16);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `major_id` int(11) NOT NULL,
  `qualtity` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `course`
--

INSERT INTO `course` (`id`, `name`, `major_id`, `qualtity`, `is_deleted`) VALUES
(1, 'Phân tích thiết kế giao diện người dùng', 1, 2, 0),
(4, 'Điều khiển vi mạch', 14, 2, 0),
(5, 'Trí Tuệ Nhân Tạo', 1, 3, 0),
(6, 'Thiết kế Website', 1, 2, 0),
(7, 'An toàn lao động', 16, 4, 0),
(8, 'Python', 17, 12, 0),
(9, 'Điều khiển vi mạch 2', 14, 12, 0),
(10, 'Test', 1, 12, 1),
(12, 'Lịch Sử Đảng Cộng Sản Việt Nam', 20, 3, 0),
(13, 'Lập Trình Web', 1, 3, 0),
(14, 'Cơ Sở Dữ Liệu', 1, 3, 0),
(15, 'Cấu Trúc Dữ Liệu Và Giải Thuật', 1, 10, 0),
(16, 'Điều khiển tự động hóa', 14, 3, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `course_regis`
--

CREATE TABLE `course_regis` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `class_section_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `course_regis`
--

INSERT INTO `course_regis` (`id`, `user_id`, `class_section_id`) VALUES
(24, 10, 21),
(25, 10, 22),
(26, 11, 21),
(27, 1, 8),
(28, 1, 19),
(29, 1, 17),
(30, 1, 5),
(31, 12, 19),
(32, 12, 20),
(33, 12, 18);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `faculty`
--

INSERT INTO `faculty` (`id`, `name`, `is_deleted`) VALUES
(1, 'Khoa Công Nghệ Số ', 0),
(2, 'Khoa Hóa Học Môi Trường', 0),
(5, 'Khoa Cơ Khí', 0),
(6, 'Khoa Xây Dựng', 1),
(7, 'Khoa Điện - Điện Tử', 0),
(9, 'Khoa Sư Phạm Công Nghiệp', 0),
(11, 'Các Môn Chung', 0),
(13, 'Test', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `major`
--

CREATE TABLE `major` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `major`
--

INSERT INTO `major` (`id`, `name`, `faculty_id`, `is_deleted`) VALUES
(1, 'Công Nghệ Thông Tin ', 1, 0),
(14, 'Tự động hóa', 7, 0),
(16, 'Xây dựng Hạ Tầng', 6, 0),
(17, 'Trí Tuệ Nhân Tạo', 1, 0),
(18, 'Guitar', 9, 0),
(20, 'Các Môn Đại Cương Lịch Sử', 11, 0),
(23, 'Test', 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Sinh Viên');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `semester`
--

CREATE TABLE `semester` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `semester`
--

INSERT INTO `semester` (`id`, `name`, `start_date`, `end_date`, `is_deleted`) VALUES
(1, 'Học kỳ 119', '2022-02-28', '2022-03-05', 0),
(2, 'Học kì 319', '2022-02-19', '2022-02-25', 0),
(3, 'Học kì 220', '2022-03-12', '2022-03-18', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DoB` date NOT NULL,
  `sex` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `name`, `DoB`, `sex`, `username`, `password`, `address`, `role_id`, `class_id`) VALUES
(1, 'Dương Miên', '2001-05-21', 'Nam', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'Đà Nẵng', 2, 13),
(2, 'Admin', '2002-01-11', 'Nữ', 'admin', '21232f297a57a5a743894a0e4a801fc3', '105 Hồ Nghinh Đà Nẵng', 1, 3),
(10, 'Dương Anh', '2001-06-12', 'Nam', 'tudonghoa', '827ccb0eea8a706c4c34a16891f84e7b', '105 Hồ Nghinh Đà Nẵng', 2, 4),
(11, 'Phan Nguyễn A', '2001-07-16', 'Nam', 'taikhoana', '827ccb0eea8a706c4c34a16891f84e7b', '105 Hồ Nghinh Đà Nẵng', 2, 4),
(12, 'Nguyễn Hạ Chi', '2001-06-16', 'Nữ', 'hachine', '827ccb0eea8a706c4c34a16891f84e7b', '110 Hồ Nghinh Đà Nẵng', 2, 3);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`),
  ADD KEY `major_id` (`major_id`);

--
-- Chỉ mục cho bảng `class_section`
--
ALTER TABLE `class_section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `semester_id` (`semester_id`),
  ADD KEY `course_id` (`course_id`);

--
-- Chỉ mục cho bảng `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `major_id` (`major_id`);

--
-- Chỉ mục cho bảng `course_regis`
--
ALTER TABLE `course_regis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `class_section_id` (`class_section_id`);

--
-- Chỉ mục cho bảng `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `major`
--
ALTER TABLE `major`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faculty_id` (`faculty_id`);

--
-- Chỉ mục cho bảng `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `class_section`
--
ALTER TABLE `class_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT cho bảng `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `course_regis`
--
ALTER TABLE `course_regis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT cho bảng `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `major`
--
ALTER TABLE `major`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `semester`
--
ALTER TABLE `semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `class`
--
ALTER TABLE `class`
  ADD CONSTRAINT `class_ibfk_1` FOREIGN KEY (`major_id`) REFERENCES `major` (`id`);

--
-- Các ràng buộc cho bảng `class_section`
--
ALTER TABLE `class_section`
  ADD CONSTRAINT `class_section_ibfk_1` FOREIGN KEY (`semester_id`) REFERENCES `semester` (`id`),
  ADD CONSTRAINT `class_section_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`);

--
-- Các ràng buộc cho bảng `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `course_ibfk_1` FOREIGN KEY (`major_id`) REFERENCES `major` (`id`);

--
-- Các ràng buộc cho bảng `course_regis`
--
ALTER TABLE `course_regis`
  ADD CONSTRAINT `course_regis_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `course_regis_ibfk_2` FOREIGN KEY (`class_section_id`) REFERENCES `class_section` (`id`);

--
-- Các ràng buộc cho bảng `major`
--
ALTER TABLE `major`
  ADD CONSTRAINT `major_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`);

--
-- Các ràng buộc cho bảng `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
