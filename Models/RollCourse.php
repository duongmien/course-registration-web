<?php
require_once("Model.php");
class RollCourse extends Model
{
    function getDetailCourse($id)
    {
        $query = "SELECT cls.*,cls.id as cls_id, c.qualtity as course_qualtity, c.name as course_name, s.name as s_name FROM class_section as cls
                    JOIN course as c on  c.id=cls.course_id
                    JOIN semester as s on s.id=cls.semester_id
                    WHERE cls.id=$id";
        $result = $this->conn->query($query);
        return $result->fetch_assoc();
    }
}
