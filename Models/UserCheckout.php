<?php
require_once("model.php");
class UserCheckout extends Model
{
    function saveCourse()
    {
        $id_user = $_SESSION['login']['id'];
        $course_qualtity = 0;
        foreach ($_SESSION['class_section'] as $value) {
            $course_qualtity = $course_qualtity + $value['course_qualtity'];
        }
        //Kiểm tra xem tổng số chỉ đăng ký đã đạt mức tối thiểu 14 chỉ và tối đa 27 chỉ chưa
        if ($course_qualtity >= 14 && $course_qualtity <= 27) {
            foreach ($_SESSION['class_section'] as $value) {
                $class_section_id = $value['id'];
                $query = $this->conn->prepare("INSERT INTO course_regis(user_id,class_section_id) 
                            VALUES (?, ?)");
                $query->bind_param("ssi", $id_user, $class_section_id);
                $status = $query->execute();
            }
            return $status;
        }
        return 0;
    }
}
