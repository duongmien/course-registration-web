<?php
require_once("model.php");
class UserLogin extends Model
{
    var $conn;
    function __construct()
    {
        $this->conn = MySQLConnection::getInstance()->conn;
    }
    function userLogin($data)
    {
        $query = "SELECT * from user  WHERE username = '" . $data['username'] . "' AND password = '" . $data['password'] . "'";
        $login = $this->conn->query($query)->fetch_assoc();
        if ($login !== NULL) {
            if ($login['role_id'] == 1) {
                $_SESSION['isLogin_Admin'] = true;
                $_SESSION['login'] = $login;
            } else {
                $_SESSION['isLogin'] = true;
                $_SESSION['login'] = $login;
            }
            setcookie('msg1', 'Đăng nhập thành công', time() + 5);
            if (isset($_SESSION['isLogin_Admin'])) {
                header('Location: Admin');
            } else {
                header('Location: ?mod=home');
            }
        } else {
            setcookie('msg1', 'Đăng nhập không thành công, sai tài khoản hoặc mật khẩu', time() + 5);
            header('Location: ?act=user#loginaction');
        }
        return $login;
    }
    function userLogout()
    {
        unset($_SESSION['login']);
        unset($_SESSION['class_section']);
        header('Location: ?mod=home');
    }
    function checkAccount()
    {
        $query =  "SELECT * from user";

        require("result.php");

        return $data;
    }
    function error()
    {
        header('location: ?act=errors');
    }
    function getAccount()
    {
        $id = $_SESSION['login']['id'];
        return $this->conn->query("SELECT * from user where id = $id")->fetch_assoc();
    }
}
