<?php
require_once("model.php");
class Schedule extends Model
{
    function getSchedule($id_user, $id_semester)
    {
        $query = "SELECT cls.*,cls.id as cls_id, c.qualtity as course_qualtity, c.name as course_name, s.name as s_name FROM course_regis as cr
        JOIN class_section as cls on  cls.id=cr.class_section_id
        JOIN course as c on  c.id=cls.course_id
        JOIN semester as s on s.id=cls.semester_id
        WHERE cr.user_id=$id_user AND s.id= $id_semester
        ORDER BY cls.day ASC";
        return $this->returnData($query);
    }
}
