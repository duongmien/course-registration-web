<?php
require_once("connection.php");
class model
{
    var $conn;
    public function __construct()
    {
        // $conn_obj = new MySQLConnection();
        // $this->conn = $conn_obj->conn;
        $this->conn = MySQLConnection::getInstance()->conn;
    }
    public function returnData($query)
    {
        $result = $this->conn->query($query);
        $data = array();
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }
}
