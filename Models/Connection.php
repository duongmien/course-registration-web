<?php

class MySQLConnection
{
    static $instance;
    public $conn;
    private function __construct()
    {
        $severname = "localhost";
        $username = "root";
        $password = "";
        $db_name = "course_registration";
        $this->conn = new mysqli($severname, $username, $password, $db_name);
        $this->conn->set_charset("utf8");
    }
    static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new MySQLConnection;
        }
        return self::$instance;
    }
}
