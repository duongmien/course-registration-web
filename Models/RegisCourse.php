<?php
require_once("Model.php");
class RegisCourse extends Model
{
    function getIDFacultybyIDUser($id)
    {
        $query = "SELECT f.id as id_f FROM user as u
                    JOIN class as cla on cla.id=u.class_id
                    JOIN major as m on m.id= cla.major_id
                    JOIN faculty as f on f.id=m.faculty_id
                    WHERE u.id=$id";
        return $this->conn->query($query)->fetch_assoc();
    }
    function getAllSemester()
    {
        $query = "SELECT * FROM semester";
        return $this->returnData($query);
    }
    function getCurrentSemester()
    {
        $query = "SELECT * FROM `semester` ORDER BY id DESC LIMIT 1;";
        return $this->conn->query($query)->fetch_assoc();
    }
    function findClassSectioninByFaculty($id, $id_s)
    {
        $query = "SELECT*,COUNT(class_section_id) as count_q,cls.id as cls_id, c.id as c_id,c.qualtity as course_qualtity, c.name as course_name, s.name as s_name FROM class_section as cls
                 JOIN course as c on cls.course_id=c.id
                 JOIN semester as s on s.id= cls.semester_id
                 JOIN major as m on c.major_id=m.id
                 JOIN faculty as f on m.faculty_id=f.id 
                 LEFT JOIN course_regis as cr on cr.class_section_id = cls.id
                WHERE f.id in ($id,11) AND  s.id=$id_s
                GROUP BY cls.id
                ORDER BY cls.day ASC";
        return $this->returnData($query);
    }
}
