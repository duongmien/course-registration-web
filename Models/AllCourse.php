<?php
require_once("Model.php");
class AllCourse extends Model
{
    public function getAllCourse()
    {
        try {
            $query = "SELECT COUNT(class_section_id) as count_q, cls.*, c.qualtity as course_qualtity, c.name as course_name, s.name as s_name FROM class_section as cls
            JOIN course as c on  c.id=cls.course_id
            JOIN semester as s on s.id=cls.semester_id
            LEFT JOIN course_regis as cr on cr.class_section_id = cls.id
            GROUP BY cls.id
            ORDER BY cls.day ASC";
            return $this->returnData($query);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }
    public function getAllSemester()
    {
        try {
            $query = "SELECT * FROM semester";
            return $this->returnData($query);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }
}
