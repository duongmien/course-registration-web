<?php
require_once("Models/RegisCourse.php");
class RegisCourseController
{
    var $regiscourse_model;
    public function __construct()
    {
        $this->regiscourse_model = new RegisCourse();
    }
    function getCourseInRegis()
    {
        $id = $_SESSION['login']['id'];
        $data_allsemester = $this->regiscourse_model->getAllSemester();
        $data_currentsemester = $this->regiscourse_model->getCurrentSemester();
        $data_idfaculty = $this->regiscourse_model->getIDFacultybyIDUser($id);
        $data = $this->regiscourse_model->findClassSectioninByFaculty($data_idfaculty['id_f'], $data_currentsemester['id']);
        require_once('Views/regiscourse/regiscourse.php');
    }
}
