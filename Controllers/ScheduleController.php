<?php
require_once("Models/Schedule.php");
require_once("Models/RegisCourse.php");
class ScheduleController
{
    var $schedule_model, $regiscourse_model;
    public function __construct()
    {
        $this->schedule_model = new Schedule();
        $this->regiscourse_model = new RegisCourse();
    }

    function getViewSchedule()
    {
        $id_user = $_SESSION['login']['id'];
        $data_currentsemester = $this->regiscourse_model->getCurrentSemester();
        $data_schedule = $this->schedule_model->getSchedule($id_user, $data_currentsemester['id']);
        require_once('Views/schedule/schedule.php');
    }
}
