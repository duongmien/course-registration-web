<?php
require_once("Models/RollCourse.php");
class RollController
{
    var $roll_course_model;
    public function __construct()
    {
        $this->roll_course_model = new RollCourse();
    }
    function addCourse()
    {
        $id = $_GET['id'];
        $count = 0;
        $data = $this->roll_course_model->getDetailCourse($id);
        if (isset($_SESSION['class_section'][$id])) {
            setcookie('msg', 'Môn học này đã có rồi - Vui lòng chọn môn học khác', time() + 2);
            header('Location:?act=registration_course');
        } else {
            $arr['id'] = $data['cls_id'];
            $arr['course_name'] = $data['course_name'];
            $arr['course_qualtity'] = $data['course_qualtity'];
            $arr['quantity'] = $data['quantity'];
            $arr['name_teacher'] = $data['name_teacher'];
            $arr['classroom'] = $data['classroom'];
            $arr['day'] = $data['day'];
            $arr['period'] = $data['period'];
            $arr['start_date'] = $data['start_date'];
            $arr['s_name'] = $data['s_name'];
            $_SESSION['class_section'][$id] = $arr;
            header('Location:?act=roll');
        }
    }
    function deleteCourse()
    {
        unset($_SESSION['class_section'][$_GET['id']]);
        header('Location:?act=roll');
    }
    function getViewRollCouse()
    {
        require_once('Views/roll/roll.php');
    }
}
