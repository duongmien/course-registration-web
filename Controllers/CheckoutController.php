<?php
require_once("Models/UserCheckout.php");
class CheckoutController
{
    var $checkout_model;
    public function __construct()
    {
        $this->checkout_model = new UserCheckout();
    }
    function  saveCourse()
    {
        if ($this->checkout_model->saveCourse()) {
            setcookie('msg', 'Đăng ký thành công', time() + 2);
            header('Location:?act=roll');
            unset($_SESSION['class_section']);
        } else {
            setcookie('msg', 'Đăng ký  thất bại - Số tín chỉ cần tối thiểu là 14 và đối đa là 27', time() + 2);
            header('Location:?act=roll');
        }
    }
}
