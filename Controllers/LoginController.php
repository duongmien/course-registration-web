<?php
require_once("Models/UserLogin.php");
class LoginController
{
    var $login_model;
    public function __construct()
    {
        $this->login_model = new UserLogin();
    }
    function userLogin()
    {
        $username = $_POST['username'];
        $password = md5($_POST['password']);
        if (strpos($username, "'") != false) {
            $username = str_replace("'", "\'", $username);
        }
        $data = array(
            'username' => $username,
            'password' => $password,
        );
        $this->login_model->userLogin($data);
    }
    function userLogout()
    {
        $this->login_model->userLogout();
    }
    function login()
    {
        require_once('Views/login/login.php');
    }
    function getAccount()
    {
        $data = $this->login_model->getAccount();
        require_once('Views/home/home.php');
    }
}
