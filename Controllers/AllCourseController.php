<?php
require_once("Models/AllCourse.php");
class AllCourseController
{
    var $all_course_model;

    public function __construct()
    {
        $this->all_course_model = new AllCourse();
    }
    function getViewAllCourse()
    {
        $data = $this->all_course_model->getAllCourse();
        $data_allsemester = $this->all_course_model->getAllSemester();
        if ($data) {
            require_once('Views/registration/registration.php');
        } else {
            include("Views/error-404.php");
        }
    }
}
