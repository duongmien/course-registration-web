<?php
session_start();
$mod = isset($_GET['act']) ? $_GET['act'] : "home";
switch ($mod) {
    case 'home':
        require_once('Controllers/HomeController.php');
        $controller_obj = new HomeController();
        $controller_obj->getViewHome();
        break;
    case 'registration':
        require_once('Controllers/AllCourseController.php');
        $controller_obj = new AllCourseController();
        $controller_obj->getViewAllCourse();
        break;
    case 'schedule':
        require_once('Controllers/ScheduleController.php');
        $controller_obj = new ScheduleController();
        $controller_obj->getViewSchedule();
        break;
    case 'registration_course':
        require_once('Controllers/RegisCourseController.php');
        $controller_obj = new RegisCourseController();
        $controller_obj->getCourseInRegis();
        break;
    case 'checkout':
        $act = isset($_GET['process']) ? $_GET['process'] : "list";
        require_once('Controllers/CheckoutController.php');
        $controller_obj = new CheckoutController();
        switch ($act) {
            case 'save':
                $controller_obj->saveCourse();
                break;
            default:
                $controller_obj->saveCourse();
                break;
        }
        break;
    case 'roll':
        $act = isset($_GET['process']) ? $_GET['process'] : "list";
        require_once('Controllers/RollController.php');
        $controller_obj = new RollController();
        switch ($act) {
            case 'list':
                $controller_obj->getViewRollCouse();
                break;
            case 'add':
                $controller_obj->addCourse();
                break;
            case 'delete':
                $controller_obj->deleteCourse();
                break;
            default:
                $controller_obj->getViewRollCouse();
                break;
        }
        break;
    case 'user':
        $act = isset($_GET['process']) ? $_GET['process'] : "login";
        require_once('Controllers/LoginController.php');
        $controller_obj = new LoginController();
        switch ($act) {
            case 'login':
                $controller_obj->login();
                break;
            case 'loginaction':
                $controller_obj->userLogin();
                break;
            case 'logout':
                $controller_obj->userLogout();
                break;
            default:
                $controller_obj->login();
                break;
        }
        break;
    case 'admin':
        require_once('Admin/index.php');
        break;
    default:
        require_once('Controllers/HomeController.php');
        $controller_obj = new Homecontroller();
        $controller_obj->getViewHome();
        break;
}
