<?php
$act = isset($_GET['act']) ? $_GET['act'] : "home";
switch ($act) {
    case "home":
        require_once("home/home.php");
        break;
    case "roll":
        require_once("roll/roll.php");
        break;
    case "checkout":
        require_once("roll/roll.php");
        break;
    case "user":
        $act = isset($_GET['process']) ? $_GET['process'] : "login";
        require_once('Controllers/LoginController.php');
        $controller_obj = new LoginController();
        switch ($act) {
            case 'login':
                require_once("login/login.php");
                break;
            default:
                require_once("login/login.php");
                break;
        }
        break;
    case 'registration':
        require_once("registration/registration.php");
        break;
    case 'registration_course':
        require_once("regiscourse/regiscourse.php");
        break;
    case 'schedule':
        require_once("schedule/schedule.php");
        break;
    default:
        require_once('error-404.php');
        break;
}
