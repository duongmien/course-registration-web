<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/x-icon" href="./assets/imgs/menu/1.jpg" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/styles/components/reset.css">
    <link rel="stylesheet" href="public/styles/components/aos.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <link rel="stylesheet" href="public/styles/components/common.css">
    <link rel="stylesheet" href="public/styles/pages/style.css">
    <title>Home</title>
</head>

<body>
    <!-- Page loader start -->
    <div class="page-loader"></div>
    <!-- Page loader end -->

    <!-- Header start -->
    <header class="header">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="logo">
                    <a href="#"><img src="public/asets/logo.jpg" alt="logo"></a>
                </div>
                <button type="button" class="nav-toggler">
                    <span></span>
                </button>
                <nav class="nav ">
                    <ul>
                        <li class="nav-item"><a href="#home">home</a></li>
                        <?php if (isset($_SESSION['login'])) { ?>
                            <li class="nav-item"> <a href="?act=registration">Các HP đang mở</a></li>
                            <li class="nav-item"> <a href="?act=registration_course">Đăng ký tín chỉ</a></li>
                            <li class="nav-item"> <a href="?act=roll">HP chọn đăng ký</a></li>
                            <li class="nav-item"> <a href="?act=schedule">Thời khóa biểu</a></li>
                            <li class="nav-item"><a href="?act=user#&process=logout">Đăng xuất</a></li>
                        <?php } else { ?>
                            <li class="nav-item"> <a href="?act=user">Đăng nhập</a></li>
                        <?php } ?>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <section class="home-section" id="home">
        <div class="home-bg"></div>
        <div class="container">
            <div class="row min-vh-100">
                <div class="home-text">
                    <h1>Course Registration Web</h1>
                    <p>Have a fun evening! We are happy to serve you</p>
                    <a href="#our-menu " class="btn btn-default">our menu</a>
                </div>
            </div>
        </div>
    </section>
    <!-- <?php $id = $_SESSION['login']['id'] ?>
    <h1> <?= $id ?> </h1> -->
    <!-- Footer end -->
    <script src="./scripts/aos.js"></script>
    <script src="public/js/script.js"></script>
</body>

</html>