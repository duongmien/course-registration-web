<?php
session_start();
$id = isset($_GET['id']) ? $_GET['id'] : 1;
$faculty_id = isset($_GET['faculty_id']) ? $_GET['faculty_id'] : 1;
$mod = isset($_GET['mod']) ? $_GET['mod'] : "user";
$act = isset($_GET['act']) ? $_GET['act'] : "admin";
switch ($mod) {
    case 'user':
        require_once('MVC/controllers/UserController.php');
        $controller_obj = new UserController();
        switch ($act) {
            case 'list':
                $controller_obj->list();
                break;
            case 'detail':
                $controller_obj->getDetail($id);
                break;
            case 'add':
                $controller_obj->add();
                break;
            case 'store':
                $controller_obj->store();
                break;
            case 'delete':
                $controller_obj->delete($id);
                break;
            case 'edit':
                $controller_obj->edit($id);
                break;
            case 'update':
                $controller_obj->update($id);
                break;
            default:
                $controller_obj->list();
                break;
        }
        break;
    case 'faculty':
        require_once('MVC/controllers/FacultyController.php');
        $controller_obj = new FacultyController();
        switch ($act) {
            case 'list':
                $controller_obj->list();
                break;
            case 'add':
                $controller_obj->add();
                break;
            case 'store':
                $controller_obj->store();
                break;
            case 'delete':
                $controller_obj->delete($id);
                break;
            case 'edit':
                $controller_obj->edit($id);
                break;
            case 'update':
                $controller_obj->update($id);
                break;
            case 'detail':
                $controller_obj->getDetail($id);
                break;
            default:
                $controller_obj->list();
                break;
        }
        break;
    case 'major':
        require_once('MVC/controllers/MajorController.php');
        $controller_obj = new MajorController();
        switch ($act) {
            case 'list':
                $controller_obj->list();
                break;
            case 'add':
                $controller_obj->add();
                break;
            case 'store':
                $controller_obj->store();
                break;
            case 'delete':
                $controller_obj->delete($id);
                break;
            case 'detail_major':
                $controller_obj->detail_major($faculty_id);
                break;
            case 'edit':
                $controller_obj->edit($id);
                break;
            case 'detail':
                $controller_obj->getDetail($id);
                break;
            case 'update':
                $controller_obj->update($id);
                break;
            default:
                $controller_obj->list();
                break;
        }
        break;
    case 'class':
        require_once('MVC/controllers/ClassController.php');
        $controller_obj = new ClassController();
        switch ($act) {
            case 'list':
                $controller_obj->list();
                break;
            case 'add':
                $controller_obj->add();
                break;
            case 'store':
                $controller_obj->store();
                break;
            case 'delete':
                $controller_obj->delete($id);
                break;
            case 'edit':
                $controller_obj->edit($id);
                break;
            case 'update':
                $controller_obj->update($id);
                break;
            case 'detail':
                $controller_obj->getDetail($id);
                break;
            case 'detail_class':
                $controller_obj->getDetailClass($faculty_id);
                break;
            default:
                $controller_obj->list();
                break;
        }
        break;
    case 'course':
        require_once('MVC/controllers/CourseController.php');
        $controller_obj = new CourseController();
        switch ($act) {
            case 'list':
                $controller_obj->list();
                break;
            case 'add':
                $controller_obj->add();
                break;
            case 'store':
                $controller_obj->store();
                break;
            case 'delete':
                $controller_obj->delete($id);
                break;
            case 'edit':
                $controller_obj->edit($id);
                break;
            case 'detail':
                $controller_obj->getDetail($id);
                break;
            case 'detail_course':
                $controller_obj->detail_course($faculty_id);
                break;
            case 'update':
                $controller_obj->update($id);
                break;
            default:
                $controller_obj->list();
                break;
        }
        break;
    case 'semester':
        require_once('MVC/controllers/SemesterController.php');
        $controller_obj = new SemesterController();
        switch ($act) {
            case 'list':
                $controller_obj->list();
                break;
            case 'add':
                $controller_obj->add();
                break;
            case 'store':
                $controller_obj->store();
                break;
            case 'delete':
                $controller_obj->delete($id);
                break;
            case 'edit':
                $controller_obj->edit($id);
                break;
            case 'update':
                $controller_obj->update($id);
                break;
            default:
                $controller_obj->list();
                break;
        }
        break;
    case 'class_section':
        require_once('MVC/controllers/ClassSectionController.php');
        $controller_obj = new ClassSectionController();
        switch ($act) {
            case 'list':
                $controller_obj->list();
                break;
            case 'add':
                $controller_obj->add();
                break;
            case 'store':
                $controller_obj->store();
                break;
            case 'delete':
                $controller_obj->delete($id);
                break;
            case 'detail':
                $controller_obj->getDetail($id);
                break;
            case 'detail_classsection':
                $controller_obj->getDetailClasssection($faculty_id);
                break;
            case 'edit':
                $controller_obj->edit($id);
                break;
            case 'update':
                $controller_obj->update($id);
                break;
            default:
                $controller_obj->list();
                break;
        }
        break;
    case 'regis_course':
        require_once('MVC/controllers/RegisCourseController.php');
        $controller_obj = new RegisCourseController();
        switch ($act) {
            case 'list':
                $controller_obj->getListUserRegisCourse();
                break;
            default:
                $controller_obj->getListUserRegisCourse();
                break;
        }
        break;
}
