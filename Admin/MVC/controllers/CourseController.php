<?php
require_once("MVC/models/course.php");
require_once("MVC/models/major.php");
require_once("MVC/models/ClassSection.php");
require_once("MVC/models/validate.php");
class CourseController
{
    var $course_model, $major_model, $val_model;
    public function __construct()
    {
        $this->course_model = new Course();
        $this->major_model = new major();
        $this->cls_model = new ClassSection();
        $this->val_model = new Validate();
    }
    public function list()
    {
        $data = $this->major_model->findMajor();
        require_once("MVC/Views/Admin/index.php");
    }
    public function detail_course($id)
    {
        $data = $this->course_model->findCourseInFaculty($id);
        if ($data) {
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function getDetail($id)
    {
        $data = $this->course_model->find($id);
        $data_cls = $this->cls_model->findClassSectionInCourse($id);
        if ($data) {
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function add()
    {
        $data_major = $this->major_model->getAllMajor();
        require_once("MVC/Views/Admin/index.php");
    }
    public function store()
    {
        if ($this->val_model->isQualtityCourse($_POST['qualtity'])) {
            $data = array(
                'name' => $_POST['name'],
                'qualtity' => $_POST['qualtity'],
                'major_id' => $_POST['major_id']
            );
            foreach ($data as $key => $value) {
                if (strpos($value, "'") != false) {
                    $value = str_replace("'", "\'", $value);
                    $data[$key] = $value;
                }
            }
            if ($this->course_model->store($data)) {
                setcookie('msg', 'Thêm mới thành công', time() + 2);
                header('Location: ?mod=' . "course");
            } else {
                setcookie('msg', 'Thêm vào không thành công', time() + 2);
                header('Location: ?mod=' .  "course" . '&act=add');
            }
        } else {
            echo '<script language="javascript">';
            echo 'alert("Vui lòng kiểm tra dữ liệu")';
            echo '</script>';
        }
    }
    public function delete($id)
    {
        if ($this->course_model->find($id) != null) {
            $this->course_model->SoftDeleted($id);
            if ($this->course_model->SoftDeleted($id)) {
                setcookie('msg', 'Xóa thành công', time() + 2);
            } else {
                setcookie('msg', 'Xóa không thành công', time() + 2);
            }
            header('Location: ?mod=' . "course");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function edit($id)
    {
        $data = $this->course_model->All();
        $data_major = $this->major_model->getAllMajor();
        if ($this->course_model->find($id)) {
            $data = $this->course_model->find($id);
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function update($id)
    {
        if ($this->val_model->isQualtityCourse($_POST['qualtity'])) {
            $data = array(
                'id' => $_POST['ID'],
                'name' => $_POST['name'],
                'qualtity' => $_POST['qualtity'],
                'major_id' => $_POST['major_id']
            );
            foreach ($data as $key => $value) {
                if (strpos($value, "'") != false) {
                    $value = str_replace("'", "\'", $value);
                    $data[$key] = $value;
                }
            }
            if ($this->course_model->update($data)) {
                setcookie('msg', 'Duyệt thành công', time() + 2);
                header('Location: ?mod=' . "course");
            } else {
                setcookie('msg', 'Thêm vào không thành công.', time() + 2);
                header('Location: ?mod=' . "course" . '&act=edit&id=' . $data['id']['id']);
            }
        } else {
            echo '<script language="javascript">';
            echo 'alert("Vui lòng kiểm tra dữ liệu")';
            echo '</script>';
        }
    }
}
