<?php
require_once("MVC/models/RegisCourse.php");
class RegisCourseController
{
    var $regis_course_model;
    public function __construct()
    {
        $this->regis_course_model = new Regiscourse();
    }
    public function getListUserRegisCourse()
    {
        $data = $this->regis_course_model->getListUserRegisCourse();
        require_once("MVC/Views/Admin/index.php");
    }
}
