<?php
require_once("MVC/models/user.php");
require_once("MVC/models/role.php");
require_once("MVC/models/class.php");
require_once("MVC/models/validate.php");
class UserController
{
    var $user_model;
    var $role_model;
    var $class_model;
    var $validate_model;
    public function __construct()
    {
        $this->user_model = new User();
        $this->role_model = new Role();
        $this->class_model = new tblClass();
        $this->validate_model = new Validate();
    }
    public function list()
    {
        $data = $this->user_model->getInfoUser();
        require_once("MVC/Views/Admin/index.php");
    }
    public function getDetail($id)
    {
        $datarole = $this->role_model->findRoleInUser($id);
        $dataclass = $this->class_model->findClassInUser($id);
        $data = $this->user_model->find($id);
        if ($data) {
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function add()
    {
        $dataclass = $this->class_model->getAllClass();
        $datarole = $this->role_model->getAllRole();
        require_once("MVC/Views/Admin/index.php");
    }
    public function store()
    {
        // if ($this->validate_model->isEmail($_POST['name'])) {
        // } else {
        //     setcookie('msg', 'Email không hợp lệ', time() + 2);
        //     header('Location: ?mod=' . "user");
        // }
        $data = array(
            'name' =>    $_POST['name'],
            'DoB'  =>   $_POST['DoB'],
            'sex' => $_POST['sex'],
            'username' => $_POST['username'],
            'password' => md5($_POST['password']),
            'address' =>    $_POST['address'],
            'role_id'  =>   $_POST['role_id'],
            'class_id' => $_POST['class_id'],
        );
        foreach ($data as $key => $value) {
            if (strpos($value, "'") != false) {
                $value = str_replace("'", "\'", $value);
                $data[$key] = $value;
            }
        }
        if ($this->user_model->store($data)) {
            setcookie('msg', 'Thêm mới thành công', time() + 2);
            header('Location: ?mod=' . "user");
        } else {
            setcookie('msg', 'Thêm vào không thành công', time() + 2);
            header('Location: ?mod=' .  "user" . '&act=add');
        }
    }
    public function delete($id)
    {
        if ($this->user_model->find($id) != null) {
            $this->user_model->delete($id);
            if ($this->user_model->delete($id)) {
                setcookie('msg', 'Xóa thành công', time() + 2);
            } else {
                setcookie('msg', 'Xóa không thành công', time() + 2);
            }
            header('Location: ?mod=' . "user");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function edit($id)
    {
        $dataclass = $this->class_model->getAllClass();
        $datarole = $this->role_model->getAllRole();
        if ($this->user_model->find($id)) {
            $data = $this->user_model->find($id);
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function update($id)
    {
        $data = array(
            'id' => $_POST['ID'],
            'name' =>    $_POST['name'],
            'DoB'  =>   $_POST['DoB'],
            'sex' => $_POST['sex'],
            'username' => $_POST['username'],
            'password' => md5($_POST['password']),
            'address' =>    $_POST['address'],
            'role_id'  =>   $_POST['role_id'],
            'class_id' => $_POST['class_id'],
        );

        foreach ($data as $key => $value) {
            if (strpos($value, "'") != false) {
                $value = str_replace("'", "\'", $value);
                $data[$key] = $value;
            }
        }
        if ($this->user_model->update($data)) {
            setcookie('msg', 'Duyệt thành công', time() + 2);
            header('Location: ?mod=' . "user");
        } else {
            setcookie('msg', 'Update vào không thành công', time() + 2);
            header('Location: ?mod=' . $this->table . '&act=edit&id=' . $data['id']['id']);
        }
    }
}
