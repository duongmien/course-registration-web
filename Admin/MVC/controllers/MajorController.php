<?php
require_once("MVC/models/major.php");
require_once("MVC/models/faculty.php");
require_once("MVC/models/class.php");
require_once("MVC/models/course.php");
require_once("MVC/models/ClassSection.php");
class MajorController
{
    var $major_model;
    public function __construct()
    {
        $this->major_model = new Major();
        $this->class_model = new tblClass();
        $this->faculty_model = new Faculty();
        $this->course_model = new Course();
        $this->cls_model = new ClassSection();
    }
    public function list()
    {
        $data = $this->faculty_model->returnFaculty();
        require_once("MVC/Views/Admin/index.php");
    }
    public function detail_major($id)
    {
        $data = $this->major_model->finMajorInFaculty($id);
        if ($data) {
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function getDetail($id)
    {
        $data_class = $this->class_model->findClassInMajor($id);
        $data_course = $this->course_model->findCourseInMajor($id);
        $data_cls = $this->cls_model->findClassSectionInMajor($id);
        $data = $this->major_model->find($id);
        if ($data) {
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function add()
    {
        $data_faculty = $this->faculty_model->getAllFaculty();
        require_once("MVC/Views/Admin/index.php");
    }
    public function store()
    {
        $data = array(
            'name' => $_POST['name'],
            'faculty_id' => $_POST['faculty_id']
        );
        foreach ($data as $key => $value) {
            if (strpos($value, "'") != false) {
                $value = str_replace("'", "\'", $value);
                $data[$key] = $value;
            }
        }
        if ($this->major_model->store($data)) {
            setcookie('msg', 'Thêm mới thành công', time() + 2);
            header('Location: ?mod=' . "major");
        } else {
            setcookie('msg', 'Thêm vào không thành công', time() + 2);
            header('Location: ?mod=' .  "major" . '&act=add');
        }
    }
    public function delete($id)
    {
        if ($this->major_model->find($id) != null) {
            $this->major_model->SoftDeleted($id);
            if ($this->major_model->SoftDeleted($id)) {
                setcookie('msg', 'Xóa thành công', time() + 2);
            } else {
                setcookie('msg', 'Xóa không thành công', time() + 2);
            }
            header('Location: ?mod=' . "major");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function edit($id)
    {
        $data = $this->major_model->All();
        $data_faculty = $this->faculty_model->getAllFaculty();
        if ($this->major_model->find($id)) {
            $data = $this->major_model->find($id);
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function update($id)
    {
        $data = array(
            'id' => $_POST['ID'],
            'name' => $_POST['name'],
            'faculty_id' => $_POST['faculty_id']
        );
        foreach ($data as $key => $value) {
            if (strpos($value, "'") != false) {
                $value = str_replace("'", "\'", $value);
                $data[$key] = $value;
            }
        }
        if ($this->major_model->update($data)) {
            setcookie('msg', 'Duyệt thành công', time() + 2);
            header('Location: ?mod=' . "major");
        } else {
            header('Location: ?mod=' . "major");
            header('Location: ?mod=' . $this->table . '&act=edit&id=' . $data['id']['id']);
        }
    }
}
