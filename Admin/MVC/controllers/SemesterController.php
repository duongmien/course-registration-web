<?php
require_once("MVC/models/semester.php");
class SemesterController
{
    var $semester_model;
    public function __construct()
    {
        $this->semester_model = new Semester();
    }
    public function getDetail($id)
    {
        $data = $this->semester_model->find($id);
        if ($data) {
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function list()
    {

        $data = $this->semester_model->All();
        require_once("MVC/Views/Admin/index.php");
    }
    public function add()
    {
        require_once("MVC/Views/Admin/index.php");
    }
    public function store()
    {
        $data = array(
            'name' => $_POST['name'],
            'start_date' => $_POST['start_date'],
            'end_date' => $_POST['end_date']
        );
        foreach ($data as $key => $value) {
            if (strpos($value, "'") != false) {
                $value = str_replace("'", "\'", $value);
                $data[$key] = $value;
            }
        }
        if ($this->semester_model->store($data)) {
            setcookie('msg', 'Thêm mới thành công', time() + 2);
            header('Location: ?mod=' . "semester");
        } else {
            setcookie('msg', 'Thêm vào không thành công', time() + 2);
            header('Location: ?mod=' .  "semester" . '&act=add');
        }
    }
    public function delete($id)
    {
        if ($this->semester_model->find($id) != null) {
            $this->semester_model->SoftDeleted($id);
            if ($this->semester_model->SoftDeleted($id)) {
                setcookie('msg', 'Xóa thành công', time() + 2);
            } else {
                setcookie('msg', 'Xóa không thành công', time() + 2);
            }
            header('Location: ?mod=' . "semester");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function edit($id)
    {
        $data = $this->semester_model->All();
        if ($this->semester_model->find($id)) {
            $data = $this->semester_model->find($id);
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function update($id)
    {
        $data = array(
            'id' => $_POST['ID'],
            'name' => $_POST['name'],
            'start_date' => $_POST['start_date'],
            'end_date' => $_POST['end_date']
        );
        foreach ($data as $key => $value) {
            if (strpos($value, "'") != false) {
                $value = str_replace("'", "\'", $value);
                $data[$key] = $value;
            }
        }
        if ($this->semester_model->update($data)) {
            setcookie('msg', 'Duyệt thành công', time() + 2);
            header('Location: ?mod=' . "semester");
        } else {
            header('Location: ?mod=' . $this->table . '&act=edit&id=' . $data['id']['id']);
        }
    }
}
