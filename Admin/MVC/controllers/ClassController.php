<?php
require_once("MVC/models/class.php");
require_once("MVC/models/major.php");
require_once("MVC/models/user.php");
class ClassController
{
    var $class_model;
    public function __construct()
    {
        $this->class_model = new tblClass();
        $this->user_model = new User();
        $this->major_model = new Major();
    }
    public function getDetailClass($id)
    {
        $data = $this->class_model->finClassInFaculty($id);
        if ($data) {
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function getDetail($id)
    {
        $data = $this->class_model->find($id);
        $data_info = $this->user_model->findUserInClass($id);
        if ($data) {
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function list()
    {
        $data = $this->major_model->findMajorInClass();
        require_once("MVC/Views/Admin/index.php");
    }
    public function add()
    {
        $data_major = $this->major_model->getAllMajor();
        require_once("MVC/Views/Admin/index.php");
    }
    public function store()
    {
        $data = array(
            'name' => $_POST['name'],
            'major_id' => $_POST['major_id']
        );
        foreach ($data as $key => $value) {
            if (strpos($value, "'") != false) {
                $value = str_replace("'", "\'", $value);
                $data[$key] = $value;
            }
        }
        if ($this->class_model->store($data)) {
            setcookie('msg', 'Thêm mới thành công', time() + 2);
            header('Location: ?mod=' . "class");
        } else {
            setcookie('msg', 'Thêm vào không thành công', time() + 2);
            header('Location: ?mod=' .  "class" . '&act=add');
        }
    }
    public function delete($id)
    {
        if ($this->class_model->find($id) != null) {
            $this->class_model->SoftDeleted($id);
            if ($this->class_model->SoftDeleted($id)) {
                setcookie('msg', 'Xóa thành công', time() + 2);
            } else {
                setcookie('msg', 'Xóa không thành công', time() + 2);
            }
            header('Location: ?mod=' . "class");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function edit($id)
    {
        $data = $this->class_model->All();
        $data_major = $this->major_model->getAllMajor();
        if ($this->class_model->find($id)) {
            $data = $this->class_model->find($id);
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function update($id)
    {
        $data = array(
            'id' => $_POST['ID'],
            'name' => $_POST['name'],
            'major_id' => $_POST['major_id']
        );
        foreach ($data as $key => $value) {
            if (strpos($value, "'") != false) {
                $value = str_replace("'", "\'", $value);
                $data[$key] = $value;
            }
        }
        if ($this->class_model->update($data)) {
            setcookie('msg', 'Duyệt thành công', time() + 2);
            header('Location: ?mod=' . "class");
        } else {
            header('Location: ?mod=' . $this->table . '&act=edit&id=' . $data['id']['id']);
        }
    }
}
