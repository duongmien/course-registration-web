<?php
require_once("MVC/models/faculty.php");
require_once("MVC/models/major.php");
require_once("MVC/models/class.php");
require_once("MVC/models/course.php");
require_once("MVC/models/ClassSection.php");
class FacultyController
{
    var $faculty_model;
    public function __construct()
    {
        $this->faculty_model = new Faculty();
        $this->major_model = new major();
        $this->class_model = new tblClass();
        $this->course_model = new Course();
        $this->cls_model = new ClassSection();
    }
    public function list()
    {
        $data = $this->faculty_model->All();
        require_once("MVC/Views/Admin/index.php");
    }
    public function add()
    {
        require_once("MVC/Views/Admin/index.php");
    }
    public function getDetail($id)
    {
        $data = $this->faculty_model->find($id);
        if ($data) {
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function store()
    {
        $data = array(
            'name' =>    $_POST['name'],
        );
        foreach ($data as $key => $value) {
            if (strpos($value, "'") != false) {
                $value = str_replace("'", "\'", $value);
                $data[$key] = $value;
            }
        }
        if ($this->faculty_model->store($data)) {
            setcookie('msg', 'Thêm mới thành công', time() + 2);
            header('Location: ?mod=' . "faculty");
        } else {
            setcookie('msg', 'Thêm vào không thành công', time() + 2);
            header('Location: ?mod=' .  "faculty" . '&act=add');
        }
    }
    public function delete($id)
    {
        if ($this->faculty_model->find($id) != null) {
            $this->faculty_model->SoftDeleted($id);
            if ($this->faculty_model->SoftDeleted($id)) {
                setcookie('msg', 'Xóa thành công', time() + 2);
            } else {
                setcookie('msg', 'Xóa không thành công', time() + 2);
            }
            header('Location: ?mod=' . "faculty");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function edit($id)
    {
        if ($this->faculty_model->find($id)) {
            $data = $this->faculty_model->find($id);
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function update($id)
    {
        $data = array(
            'id' => $_POST['ID'],
            'name' => $_POST['name'],
        );

        foreach ($data as $key => $value) {
            if (strpos($value, "'") != false) {
                $value = str_replace("'", "\'", $value);
                $data[$key] = $value;
            }
        }
        if ($this->faculty_model->update($data)) {
            setcookie('msg', 'Duyệt thành công', time() + 2);
            header('Location: ?mod=' . "faculty");
        } else {
            setcookie('msg', 'Update vào không thành công', time() + 2);
            header('Location: ?mod=' . $this->table . '&act=edit&id=' . $data['id']['id']);
        }
    }
}
