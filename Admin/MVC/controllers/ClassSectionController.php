<?php
require_once("MVC/models/ClassSection.php");
require_once("MVC/models/course.php");
require_once("MVC/models/semester.php");
require_once("MVC/models/validate.php");
class ClassSectionController
{
    var $cls_model, $course_model, $semester_model, $val_model;
    public function __construct()
    {
        $this->cls_model = new ClassSection();
        $this->course_model = new Course();
        $this->semester_model = new semester();
        $this->val_model = new Validate();
    }
    public function getDetail($id)
    {
        $data_course = $this->course_model->findCourseInClassSection($id);
        $data_semester = $this->semester_model->findSemesterInnClassSection($id);
        $data_count_qualtity = $this->cls_model->countQuatityClassSection($id);
        $data_student_regis = $this->cls_model->getAllStudentRegisClassSection($id);
        $data = $this->cls_model->find($id);
        if ($data || $data_course || $data_student_regis || $data_semester || $data_count_qualtity) {
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function getDetailClasssection($id)
    {
        $data_info = $this->cls_model->findClassSectioninFaculty($id);
        if ($data_info) {
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function list()
    {
        $data = $this->cls_model->getAllClassSection();
        require_once("MVC/Views/Admin/index.php");
    }
    public function add()
    {
        $data_allcourse = $this->course_model->getAllCourse();
        $data_allsemester = $this->semester_model->getAllSemester();
        require_once("MVC/Views/Admin/index.php");
    }
    public function store()
    {
        if ($this->val_model->isQualtityClassSection($_POST['quantity']) && $this->val_model->isDay($_POST['day'])) {
            $data = array(
                'quantity' => $_POST['quantity'],
                'name_teacher' => $_POST['name_teacher'],
                'classroom' => $_POST['classroom'],
                'start_date' => $_POST['start_date'],
                'day' => $_POST['day'],
                'period' => $_POST['period'],
                'semester_id' => $_POST['semester_id'],
                'course_id' => $_POST['course_id']
            );
            foreach ($data as $key => $value) {
                if (strpos($value, "'") != false) {
                    $value = str_replace("'", "\'", $value);
                    $data[$key] = $value;
                }
            }
            if ($this->cls_model->store($data)) {
                setcookie('msg', 'Thêm mới thành công', time() + 2);
                header('Location: ?mod=' . "class_section");
            } else {
                setcookie('msg', 'Thêm vào không thành công', time() + 2);
                header('Location: ?mod=' .  "class_section" . '&act=add');
            }
        } else {
            echo '<script language="javascript">';
            echo 'alert("Vui lòng kiểm tra dữ liệu")';
            echo '</script>';
        }
    }
    public function delete($id)
    {
        if ($this->cls_model->find($id) != null) {
            $this->cls_model->delete($id);
            if ($this->cls_model->delete($id)) {
                setcookie('msg', 'Xóa thành công', time() + 2);
            } else {
                setcookie('msg', 'Xóa không thành công', time() + 2);
            }
            header('Location: ?mod=' . "class_section");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function edit($id)
    {
        $data = $this->cls_model->getAllClassSection();
        $data_allcourse = $this->course_model->getAllCourse();
        $data_allsemester = $this->semester_model->getAllSemester();
        if ($this->cls_model->find($id)) {
            $data = $this->cls_model->find($id);
            require_once("MVC/Views/Admin/index.php");
        } else {
            include("MVC/Views/Error/error.php");
        }
    }
    public function update($id)
    {
        if ($this->val_model->isQualtityClassSection($_POST['quantity']) &&  $this->val_model->isDay($_POST['day'])) {
            $data = array(
                'id' => $_POST['ID'],
                'quantity' => $_POST['quantity'],
                'name_teacher' => $_POST['name_teacher'],
                'classroom' => $_POST['classroom'],
                'start_date' => $_POST['start_date'],
                'day' => $_POST['day'],
                'period' => $_POST['period'],
                'semester_id' => $_POST['semester_id'],
                'course_id' => $_POST['course_id']
            );
            foreach ($data as $key => $value) {
                if (strpos($value, "'") != false) {
                    $value = str_replace("'", "\'", $value);
                    $data[$key] = $value;
                }
            }
            if ($this->cls_model->update($data)) {
                setcookie('msg', 'Duyệt thành công', time() + 2);
                header('Location: ?mod=' . "class_section");
            } else {
                header('Location: ?mod=' . $this->table . '&act=edit&id=' . $data['id']['id']);
            }
        } else {
            echo '<script language="javascript">';
            echo 'alert("Vui lòng kiểm tra dữ liệu")';
            echo '</script>';
        }
    }
}
