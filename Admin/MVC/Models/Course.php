<?php
require_once("model.php");
require_once("MVC/models/course.php");
class Course extends Model
{
    var $table = "course";
    var $contens = "id";
    function getAllCourse()
    {
        $query = "select * from course where is_deleted=0";
        return $this->returnData($query);
    }
    // Hien thi ten hoc phan thuoc khoa khi click vao Faculty:XemChiTiet->DanhSachHocPhan
    function findCourseInFaculty($id)
    {
        $query = "SELECT *, c.name as course_name FROM course as c
                JOIN major as m on c.major_id=m.id
                 JOIN faculty as f on m.faculty_id=f.id 
                WHERE f.id=$id";
        return $this->returnData($query);
    }
    function findCourseInClassSection($id)
    {
        $query = "SELECT c.name FROM course as c
                INNER JOIN class_section AS cls ON cls.course_id=c.id WHERE cls.id=$id ";
        return $this->conn->query($query)->fetch_assoc();
    }
    function findCourseInMajor($id)
    {
        $query = "SELECT * FROM course WHERE major_id=$id";
        return $this->returnData($query);
    }
}
