<?php
require_once("model.php");
class Role extends Model
{
    var $table = "role";
    var $contens = "id";
    function getAllRole()
    {
        $query = "select * from role";
        return $this->returnData($query);
    }
    function findRoleInUser($id)
    {
        $query = "SELECT r.name FROM ROLE as r
                INNER JOIN user AS u ON u.role_id=r.id WHERE u.id=$id ";
        return $this->conn->query($query)->fetch_assoc();
    }
}
