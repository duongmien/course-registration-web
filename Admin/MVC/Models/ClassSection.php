<?php
require_once("model.php");
class ClassSection extends Model
{
    var $table = "class_section";
    var $contens = "id";
    function getAllClassSection()
    {
        $query = "SELECT cls.*, c.qualtity as course_qualtity, c.name as course_name, s.name as s_name FROM class_section as cls
                JOIN course as c on  c.id=cls.course_id
                JOIN semester as s on s.id=cls.semester_id
                ORDER BY cls.day ASC";
        return $this->returnData($query);
    }
    function findClassSectioninFaculty($id)
    {
        $query = "SELECT *,c.id as c_id, c.name as c_name, s.name as s_name FROM class_section as cls
                 JOIN course as c on cls.course_id=c.id
                 JOIN semester as s on s.id= cls.semester_id
                 JOIN major as m on c.major_id=m.id
                 JOIN faculty as f on m.faculty_id=f.id 
                WHERE f.id in ($id,11)";
        return $this->returnData($query);
    }
    function findClassSectionInMajor($id)
    {
        $query = "SELECT *, c.name as course_name, s.name as s_name FROM class_section as cls
                JOIN course as c on c.id=cls.course_id
                JOIN major as m on c.major_id=m.id
                JOIN semester as s on cls.semester_id=s.id
                WHERE m.id=$id";
        return $this->returnData($query);
    }
    function findClassSectionInCourse($id)
    {
        $query = "SELECT *,c.name as course_name,  c.qualtity as course_qualtity, s.name as s_name FROM class_section as cls
                JOIN course as c on c.id=cls.course_id
                JOIN major as m on c.major_id=m.id
                JOIN semester as s on cls.semester_id=s.id
                WHERE c.id=$id";
        return $this->returnData($query);
    }
    function countQuatityClassSection($id_class_section)
    {
        $query = " SELECT COUNT(class_section_id) as count_q FROM class_section as cls
                    JOIN course_regis as cr on cr.class_section_id = cls.id
                    WHERE cls.id=$id_class_section
                    GROUP BY cls.id";
        return $this->conn->query($query)->fetch_assoc();
    }
    function getAllStudentRegisClassSection($id_class_section)
    {
        $query = " SELECT u.*, c.name as class_name FROM class_section as cls
                JOIN course_regis as cr on cr.class_section_id = cls.id
                JOIN user as u on cr.user_id=u.id 
                JOIN class as c on c.id=u.class_id
                WHERE cls.id=$id_class_section";
        return $this->returnData($query);
    }
}
