<?php
require_once("model.php");
class Faculty extends Model
{
    var $table = "faculty";
    var $contens = "id";
    function getAllFaculty()
    {
        $query = "SELECT * from faculty where is_deleted=0";
        return $this->returnData($query);
    }

    function returnFaculty()
    {
        $query = "SELECT major.id,major.name,faculty.name as faculty_name FROM major,faculty WHERE major.faculty_id=faculty.id and major.is_deleted=0";
        return $this->returnData($query);
    }
}
