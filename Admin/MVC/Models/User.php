<?php
require_once("model.php");
class User extends Model
{
    var $table = "user";
    var $contens = "id";

    function getInfoUser()
    {
        $query = "SELECT u.* ,c.name as class_name, r.name as role_name FROM user as u
                JOIN class as c on  c.id=u.class_id
                JOIN role as r on r.id=u.role_id";
        return $this->returnData($query);
    }
    function findUserInClass($id)
    {
        $query = "SELECT u.*, c.name as class_name, r.name as role_name FROM user as u
        JOIN class as c on  c.id=u.class_id
        JOIN role as r on r.id=u.role_id
        WHERE u.class_id=$id AND u.role_id=2";
        // return $this->conn->query($query)->fetch_assoc();
        return $this->returnData($query);
    }
}
