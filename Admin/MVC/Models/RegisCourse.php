<?php
require_once("model.php");
class RegisCourse extends Model
{
    var $table = "course_regis";
    var $contens = "id";
    function getListUserRegisCourse()
    {
        $query = "SELECT *, c.qualtity as course_qualtity, c.name as course_name, s.name as s_name, u.name as user_name, u.id as user_id, cls.id as cls_id
                 FROM course_regis as r
                JOIN user as u on r.user_id=u.id 
                JOIN class_section as cls on cls.id=r.class_section_id 
                JOIN course as c on  c.id=cls.course_id
                JOIN semester as s on s.id=cls.semester_id ";
        return $this->returnData($query);
    }
}
