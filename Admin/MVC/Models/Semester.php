<?php
require_once("model.php");
class Semester extends Model
{
    var $table = "semester";
    var $contens = "id";
    function getAllSemester()
    {
        $query = "SELECT * FROM semester  where is_deleted=0";
        return $this->returnData($query);
    }
    function findSemesterInnClassSection($id)
    {
        $query = "SELECT s.name FROM semester as s
                INNER JOIN class_section AS cls ON cls.semester_id=s.id WHERE cls.id=$id ";
        return $this->conn->query($query)->fetch_assoc();
    }
}
