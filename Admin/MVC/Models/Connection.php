<?php
// Basic
// class MySQLConnection
// {
//     var $conn;

//     function __construct()
//     {
//         //Thong so ket noi CSDL
//         $severname = "localhost";
//         $username = "root";
//         $password = "";
//         $db_name = "demouser";

//         //Tao ket noi CSDL
//         $this->conn = new mysqli($severname, $username, $password, $db_name);
//         $this->conn->set_charset("utf8");

//         //check connection
//         if ($this->conn->connect_error) {
//             die("Connection failed: " . $this->conn->connect_error);
//         }
//     }
// }

// Singleton Pattern
class MySQLConnection {
    static $instance;
    public $conn;
   
    private function __construct()
    {
        $severname = "localhost";
        $username = "root";
        $password = "";
        $db_name = "course_registration";
        $this->conn = new mysqli($severname, $username, $password, $db_name);
        $this->conn->set_charset("utf8");
    }
    static function getInstance(){
        if(!isset(self::$instance)){
            self::$instance=new MySQLConnection;
        }
        return self::$instance;
    }
}

// Solid
// interface Database
// {
//     public function connectToDatabase($severname,$username,$password,$db_name);
// }
 
// class MySQLConnection implements Database
// {
//     var $conn;
//     public function connectToDatabase($severname,$username,$password,$db_name)
//     {
//         $this->conn = new mysqli($severname,$username,$password,$db_name);
//         $this->conn->set_charset("utf8");
//         if ($this->conn->connect_error) {
//             die("Connection failed: " . $this->conn->connect_error);
//         }
//     }
// }
 
// class UserDB {
//     public function __construct(Database $dbConnection)
//     {
//         $severname ="localhost"; 
//         $username ="root";
//         $password =""; 
//         $db_name ="demouser";
//         $this->$dbConnection = $dbConnection->connectToDatabase($severname,$username,$password,$db_name);
//     }
// }
