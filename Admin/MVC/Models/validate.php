<?php
class Validate
{
    function isEmail($str)
    {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str));
    }
    function isUsername($str)
    {
        return (!preg_match("/^[a-z0-9_-]{3,50}$/ix", $str));
    }
    function isQualtityCourse($number)
    {
        return ($number > 0 && $number <= 12);
    }
    function isQualtityClassSection($number)
    {
        return ($number > 0 && $number <= 120);
    }
    function isDay($number)
    {
        return ($number >= 1  && $number <= 7);
    }
    function checkDate($date)
    {
        return (date("Y") - $date >= 18);
    }
}
