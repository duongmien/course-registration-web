<?php
require_once("connection.php");
class Model
{
    var $conn;
    var $table;
    var $contens;
    function __construct()
    {
        // $conn_obj = new MySQLConnection();
        // $this->conn = $conn_obj->conn;
        $this->conn = MySQLConnection::getInstance()->conn;
    }
    function All()
    {
        $query = "select * from $this->table where is_deleted=0 ORDER BY $this->contens DESC ";
        return $this->returnData($query);
    }
    function find($id)
    {
        $query = "select * from $this->table where $this->contens =$id";
        return $this->conn->query($query)->fetch_assoc();
    }
    function delete($id)
    {
        $query = "DELETE from $this->table where $this->contens=$id";
        $status = $this->conn->query($query);
        return $status;
    }
    function SoftDeleted($id)
    {
        $query = "UPDATE $this->table SET is_deleted=1 WHERE $this->contens=$id";
        $status = $this->conn->query($query);
        return $status;
    }
    function store($data)
    {
        $f = "";
        $v = "";
        foreach ($data as $key => $value) {
            $f .= $key . ",";
            $v .= "'" . $value . "',";
        }
        $f = trim($f, ",");
        $v = trim($v, ",");
        $query = "INSERT INTO $this->table($f) VALUES ($v);";
        $status = $this->conn->query($query);
        return $status;
    }
    function update($data)
    {
        $v = "";
        foreach ($data as $key => $value) {
            $v .= $key . "='" . $value . "',";
        }
        $v = trim($v, ",");
        $query = "UPDATE $this->table SET  $v   WHERE $this->contens = " . $data[$this->contens];
        $result = $this->conn->query($query);
        return $result;
    }
    public function returnData($query)
    {
        $result = $this->conn->query($query);
        $data = array();
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }
}
