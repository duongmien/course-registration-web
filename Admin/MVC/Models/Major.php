<?php
require_once("model.php");
class Major extends Model
{
    var $table = "major";
    var $contens = "id";
    function getAllMajor()
    {
        $query = "select * from major where is_deleted=0";
        return $this->returnData($query);
    }
    function findMajor()
    {
        $query = "SELECT course.id,course.name,major.name as major_name, course.qualtity FROM major,course WHERE course.major_id=major.id and course.is_deleted=0";
        return $this->returnData($query);
    }
    function findMajorInClass()
    {
        $query = "SELECT class.id,class.name,major.name as major_name FROM major,class WHERE class.major_id=major.id and class.is_deleted=0";
        return $this->returnData($query);
    }
    // Hien thi ten Major khi click vao Faculty:XemChiTiet->DanhSachNganh
    function finMajorInFaculty($id)
    {
        $query = "SELECT m.name as major_name FROM major as m
                JOIN faculty as f on m.faculty_id=f.id 
                WHERE f.id= $id ";
        return $this->returnData($query);
    }
}
