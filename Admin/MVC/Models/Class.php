<?php
require_once("model.php");
class tblClass extends Model
{
    var $table = "class";
    var $contens = "id";
    function getAllClass()
    {
        $query = "select * from class";
        return $this->returnData($query);
    }
    // Hien thi ten Class khi click vao Faculty:XemChiTiet->DanhSachLop
    function finClassInFaculty($id)
    {
        $query = "SELECT *, c.name as class_name FROM class as c
                 JOIN major as m on c.major_id=m.id
                 JOIN faculty as f on m.faculty_id=f.id 
                WHERE f.id=$id";
        return $this->returnData($query);
    }
    function findClassInUser($id)
    {
        $query = "SELECT c.name FROM class as c
                INNER JOIN user AS u ON u.class_id=c.id WHERE u.id=$id ";
        return $this->conn->query($query)->fetch_assoc();
    }
    function findClassInMajor($id)
    {
        $query = "SELECT * from Class where major_id=$id";
        return $this->returnData($query);
    }
}
