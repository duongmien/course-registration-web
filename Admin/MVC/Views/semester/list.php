<a href="?mod=semester&act=add" type="button" class="btn btn-primary">Thêm mới</a>
<?php if (isset($_COOKIE['msg'])) { ?>
  <div class="alert alert-success">
    <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
  </div>
<?php } ?>
<hr>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th scope="col">Mã Học Kỳ</th>
      <th scope="col">Tên Học Kỳ</th>
      <th scope="col">Thời gian bắt đầu</th>
      <th scope="col">Thời gian kết thúc</th>
      <th>#</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($data as $row) { ?>
      <tr>
        <th scope="row"><?= $row['id'] ?></th>
        <td><?= $row['name'] ?></td>
        <td><?= $row['start_date'] ?></td>
        <td><?= $row['end_date'] ?></td>
        <td>
          <a href="?mod=semester&act=detail&id=<?= $row['id'] ?>" type="button" class="btn btn-success">Xem</a>
          <a href="?mod=semester&act=edit&id=<?= $row['id'] ?>" type="button" class="btn btn-warning">Sửa</a>
          <a href="?mod=semester&act=delete&id=<?= $row['id'] ?>" onclick="return confirm('Bạn có thật sự muốn xóa ?');" type="button" class="btn btn-danger">Xóa</a>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(document).ready(function() {
    $('#dataTable').DataTable();
  });
</script>