<?php if (isset($_COOKIE['msg'])) { ?>
    <div class="alert alert-success">
        <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
    </div>
<?php } ?>
<hr>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <form action="?mod=semester&act=update" method="POST" role="form" enctype="multipart/form-data">
        <input type="hidden" name="ID" value="<?= $data['id'] ?>">
        <div class="form-group">
            <label for="">Tên Học Kỳ:</label>
            <input type="text" class="form-control" id="" placeholder="" name="name" value="<?= $data['name'] ?>">
        </div>
        <div class="form-group">
            <label for="">Thời gian bắt đầu:</label>
            <input type="date" class="form-control" id="" placeholder="" name="start_date" value="<?= $data['start_date'] ?>">
        </div>
        <div class="form-group">
            <label for="">Thời gian kết thúc:</label>
            <input type="date" class="form-control" id="" placeholder="" name="end_date" value="<?= $data['end_date'] ?>">
        </div>
        <button id="submit-btn" class="btn btn-primary">Create</button>
    </form>
    </tbody>
</table>