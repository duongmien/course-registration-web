   <?php if (isset($_COOKIE['msg'])) { ?>
       <div class="alert alert-success">
           <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
       </div>
   <?php } ?>
   <hr>
   <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
       <?php if (isset($_COOKIE['msg'])) { ?>
           <div class="alert alert-warning">
               <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
           </div>
       <?php } ?>
       <form action="?mod=user&act=store" method="POST" role="form" enctype="multipart/form-data">
           <div class="form-group">
               <label for="">Họ Tên</label>
               <input type="text" class="form-control" id="" placeholder="" name="name">
           </div>
           <div class="form-group">
               <label for="">Ngày sinh</label>
               <input type="date" class="form-control" id="" placeholder="" name="DoB">
           </div>
           <div class="form-group">
               <label for="">Giới Tính</label>
               <select id="" name="sex" class="form-control">
                   <option value="Nam"> Nam</option>
                   <option value="Nữ"> Nữ</option>
                   <option value="Khác"> Khác</option>
               </select>
           </div>
           <div class="form-group">
               <label for="">Lớp</label>
               <select id="" name="class_id" class="form-control">
                   <?php foreach ($dataclass as $row) { ?>
                       <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                   <?php } ?>
               </select>
           </div>
           <div class="form-group">
               <label for="">Tên Tài Khoản</label>
               <input type="text" class="form-control" id="" placeholder="" name="username">
           </div>
           <div class="form-group">
               <label for="">Mật Khẩu</label>
               <input type="text" class="form-control" id="" placeholder="" name="password">
           </div>
           <div class="form-group">
               <label for="">Địa Chỉ</label>
               <input type="text" class="form-control" id="" placeholder="" name="address">
           </div>
           <div class="form-group">
               <label for="">Quyền</label>
               <select id="" name="role_id" class="form-control">
                   <?php foreach ($datarole as $rowr) { ?>
                       <option value="<?= $rowr['id'] ?>"><?= $rowr['name'] ?></option>
                   <?php } ?>
               </select>
           </div>
           <button type="submit" class="btn btn-primary">Create</button>
       </form>
   </table>