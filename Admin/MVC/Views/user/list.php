
<a href="?mod=user&act=add" type="button" class="btn btn-primary">Thêm mới</a>
<?php if (isset($_COOKIE['msg'])) { ?>
  <div class="alert alert-success">
    <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
  </div>
<?php } ?>
<hr>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Họ Tên</th>
      <th scope="col">Ngày sinh</th>
      <th scope="col">Giới Tính</th>
      <th scope="col">Username</th>
      <th scope="col">Lớp</th>
      <th scope="col">Quyền</th>
      <th>#</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($data as $row) { ?>
      <tr>
        <th scope="row"><?= $row['id'] ?></th>
        <td><?= $row['name'] ?></td>
        <td><?= $row['DoB'] ?></td>
        <td><?= $row['sex'] ?></td>
        <td><?= $row['username'] ?></td>
        <td><?= $row['class_name'] ?></td>
        <td><?= $row['role_name'] ?></td>
        <td>
          <a href="?mod=user&act=detail&id=<?= $row['id'] ?>" type="button" class="btn btn-success">Xem</a>
          <a href="?mod=user&act=edit&id=<?= $row['id'] ?>" type="button" class="btn btn-warning">Sửa</a>
          <a href="?mod=user&act=delete&id=<?= $row['id'] ?>" onclick="return confirm('Bạn có thật sự muốn xóa ?');" type="button" class="btn btn-danger">Xóa</a>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(document).ready(function() {
    $('#dataTable').DataTable();
  });
</script>