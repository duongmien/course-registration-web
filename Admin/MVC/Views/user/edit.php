<?php if (isset($_COOKIE['msg'])) { ?>
    <div class="alert alert-success">
        <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
    </div>
<?php } ?>
<hr>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <form action="?mod=user&act=update" method="POST" role="form" enctype="multipart/form-data">
        <input type="hidden" name="ID" value="<?= $data['id'] ?>">
        <div class="form-group">
            <label for="">Họ Tên</label>
            <input type="text" class="form-control" id="" placeholder="" name="name" value="<?= $data['name'] ?>">
        </div>
        <div class="form-group">
            <label for="">Ngày sinh</label>
            <input type="date" class="form-control" id="" placeholder="" name="DoB" value="<?= $data['DoB'] ?>">
        </div>
        <div class="form-group">
            <label for="">Giới tính</label>
            <select id="" name="sex" class="form-control">
                <option <?= ($data['sex'] == 'Nam') ? 'selected' : '' ?> value="Nam"> Nam</option>
                <option <?= ($data['sex'] == 'Nữ') ? 'selected' : '' ?> value="Nữ"> Nữ</option>
                <option <?= ($data['sex'] == 'Khác') ? 'selected' : '' ?> value="Khác"> Khác</option>
            </select>
        </div>
        <div class="form-group">
            <label for="">Lớp</label>
            <select id="" name="class_id" class="form-control">
                <?php foreach ($dataclass as $row_c) { ?>
                    <option <?= ($row_c['id'] == $data['class_id']) ? 'selected' : '' ?> value="<?= $row_c['id'] ?>"><?= $row_c['name'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="">Tên Tài Khoản</label>
            <input type="text" class="form-control" id="" placeholder="" name="username" value="<?= $data['username'] ?>">
        </div>
        </div>
        <div class="form-group">
            <label for="">Mật Khẩu</label>
            <input type="text" class="form-control" id="" placeholder="" name="password" value="<?= md5($data['password']) ?>">
        </div>
        </div>
        <div class="form-group">
            <label for="">Địa Chỉ</label>
            <input type="text" class="form-control" id="" placeholder="" name="address" value="<?= $data['address'] ?>">
        </div>
        </div>
        <div class="form-group">
            <label for="">Quyền</label>
            <select id="" name="role_id" class="form-control">
                <?php foreach ($datarole as $row) { ?>
                    <option <?= ($row['id'] == $data['role_id']) ? 'selected' : '' ?> value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                <?php } ?>
            </select>
        </div>
        <button id="submit-btn" class="btn btn-primary">Create</button>
    </form>
    </tbody>
</table>