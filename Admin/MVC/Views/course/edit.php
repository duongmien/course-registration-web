<?php if (isset($_COOKIE['msg'])) { ?>
    <div class="alert alert-success">
        <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
    </div>
<?php } ?>
<hr>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <form action="?mod=course&act=update" method="POST" role="form" enctype="multipart/form-data">
        <input type="hidden" name="ID" value="<?= $data['id'] ?>">
        <div class="form-group">
            <label for="">Tên Học Phần:</label>
            <input type="text" class="form-control" id="" placeholder="" name="name" value="<?= $data['name'] ?>">
        </div>
        <div class="form-group">
            <label for="">Số tín chỉ:</label>
            <input type="int" class="form-control" id="" placeholder="" name="qualtity" value="<?= $data['qualtity'] ?>">
        </div>
        <div class="form-group">
            <label for="cars">Chọn Ngành: </label>
            <select id="" name="major_id" class="form-control">
                <?php foreach ($data_major as $row) { ?>
                    <option <?= ($row['id'] == $data['major_id'])?'selected':''?> value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                <?php } ?>
            </select>
        </div>
        <button id="submit-btn" class="btn btn-primary">Create</button>
    </form>
    </tbody>
</table>