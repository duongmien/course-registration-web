<a href="?mod=course&act=add" type="button" class="btn btn-primary">Thêm mới</a>
<?php if (isset($_COOKIE['msg'])) { ?>
  <div class="alert alert-success">
    <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
  </div>
<?php } ?>
<hr>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th scope="col">Mã Học Phần</th>
      <th scope="col">Tên Học Phần</th>
      <th scope="col">Số Tín Chỉ</th>
      <th scope="col">Tên Ngành</th> 
      <th>#</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($data as $row) { ?>
      <tr>
        <th scope="row"><?= $row['id'] ?></th>
        <td><?= $row['name'] ?></td>
        <td><?= $row['qualtity'] ?></td>
        <td><?= $row['major_name'] ?></td>
        <td>
          <a href="?mod=course&act=detail&id=<?= $row['id'] ?>" type="button" class="btn btn-success">Xem</a>
          <a href="?mod=course&act=edit&id=<?= $row['id'] ?>" type="button" class="btn btn-warning">Sửa</a>
          <a href="?mod=course&act=delete&id=<?= $row['id'] ?>" onclick="return confirm('Bạn có thật sự muốn xóa ?');" type="button" class="btn btn-danger">Xóa</a>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(document).ready(function() {
    $('#dataTable').DataTable();
  });
</script>