<h1>THÔNG TIN LỚP HỌC PHẦN</h1>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <h2>Mã Lớp Học Phần: <?= $data['id'] ?></h2>
    <h2>Tên Học Phần: <?= $data_course['name'] ?></h2>
    <h2>Số Lượng : <?= $data['quantity'] ?></h2>
    <h2>Giảng viên : <?= $data['name_teacher'] ?></h2>
    <h2>Phòng học : <?= $data['classroom'] ?></h2>
    <h2>Thứ học : <?= $data['day'] ?></h2>
    <h2>Tiết học : <?= $data['period'] ?></h2>
    <h2>Ngày bắt đầu : <?= $data['start_date'] ?></h2>
    <h2>Học kỳ : <?= $data_semester['name'] ?></h2>
</table>
<h1>Số lượng sinh viên đã đăng ký lớp học phần : <?= $data_count_qualtity['count_q'] ?></h1>
<h1>DANH SÁCH SINH VIÊN ĐÃ ĐĂNG KÝ LỚP HỌC PHẦN</h1>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th scope="col">STT</th>
            <th scope="col">ID</th>
            <th scope="col">Họ Tên</th>
            <th scope="col">Ngày sinh</th>
            <th scope="col">Giới Tính</th>
            <th scope="col">Lớp</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; ?>
        <?php foreach ($data_student_regis  as $row) { ?>
            <tr>
                <td><?= $i++ ?></td>
                <th scope="row"><?= $row['id'] ?></th>
                <td><?= $row['name'] ?></td>
                <td><?= $row['DoB'] ?></td>
                <td><?= $row['sex'] ?></td>
                <td><?= $row['class_name'] ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>