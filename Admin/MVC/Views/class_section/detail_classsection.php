<h1><i>Các Lớp Học Phần Đang Mở : </i></h1>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th scope="col">Mã LHP</th>
            <th scope="col">Tên HP</th>
            <th scope="col">Số lượng</th>
            <th scope="col">Giảng viên</th>
            <th scope="col">Phòng học</th>
            <th scope="col">Thứ học</th>
            <th scope="col">Tiết học</th>
            <th scope="col">Ngày bắt đầu</th>
            <th scope="col">Học Kỳ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data_info as $row1) { ?>
            <tr>
                <td scope="row"><?= $row1['c_id'] ?></td>
                <td><?= $row1['c_name'] ?></td>
                <td><?= $row1['quantity'] ?></td>
                <td><?= $row1['name_teacher'] ?></td>
                <td><?= $row1['classroom'] ?></td>
                <td><?= $row1['day'] ?></td>
                <td><?= $row1['period'] ?></td>
                <td><?= $row1['start_date'] ?></td>
                <td><?= $row1['s_name'] ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>