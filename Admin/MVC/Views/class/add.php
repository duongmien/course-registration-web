   <?php if (isset($_COOKIE['msg'])) { ?>
       <div class="alert alert-success">
           <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
       </div>
   <?php } ?>
   <hr>
   <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
       <?php if (isset($_COOKIE['msg'])) { ?>
           <div class="alert alert-warning">
               <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
           </div>
       <?php } ?>
       <form action="?mod=class&act=store" method="POST" role="form" enctype="multipart/form-data">
           <div class="form-group">
               <label for="">Tên Lớp</label>
               <input type="text" class="form-control" id="" placeholder="" name="name">
           </div>
           <div class="form-group">
               <label for="cars">Chọn Ngành: </label>
               <select id="" name="major_id" class="form-control">
                   <?php foreach ($data_major as $row) { ?>
                       <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                   <?php } ?>
               </select>
           </div>
           <button type="submit" class="btn btn-primary">Create</button>
       </form>
   </table>