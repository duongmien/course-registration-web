<a href="?mod=class_section&act=add" type="button" class="btn btn-primary">Thêm mới</a>
<?php if (isset($_COOKIE['msg'])) { ?>
    <div class="alert alert-success">
        <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
    </div>
<?php } ?>
<hr>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th scope="col">Mã Sinh Viên</th>
            <th scope="col">Tên Sinh Viên</th>
            <th scope="col">Mã LHP</th>
            <th scope="col">Tên HP</th>
            <th scope="col">Số tín chỉ</th>
            <th scope="col">Số lượng</th>
            <th scope="col">Giảng viên</th>
            <th scope="col">Phòng học</th>
            <th scope="col">Thứ học</th>
            <th scope="col">Tiết học</th>
            <th scope="col">Ngày bắt đầu</th>
            <th scope="col">Học Kỳ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $row) { ?>
            <tr>
                <td><?= $row['user_id'] ?></td>
                <td><?= $row['user_name'] ?></td>
                <td scope="row"><?= $row['cls_id'] ?></td>
                <td><?= $row['course_name'] ?></td>
                <td><?= $row['course_qualtity'] ?></td>
                <td><?= $row['quantity'] ?></td>
                <td><?= $row['name_teacher'] ?></td>
                <td><?= $row['classroom'] ?></td>
                <td><?= "Thứ " . $row['day'] ?></td>
                <td><?= $row['period'] ?></td>
                <td><?= $row['start_date'] ?></td>
                <td><?= $row['s_name'] ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>