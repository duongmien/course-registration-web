 <!-- Sidebar -->
 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

   <!-- Sidebar - Brand -->
   <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
     <div class="sidebar-brand-icon rotate-n-15">
       <i class="fas fa-laugh-wink"></i>
     </div>
     <div class="sidebar-brand-text mx-3">MIE<sup>Shop</sup></div>
   </a>

   <!-- Divider -->
   <hr class="sidebar-divider my-0">

   <!-- Divider -->
   <hr class="sidebar-divider">

   <!-- Heading -->
   <div class="sidebar-heading">
     Chức năng
   </div>


   <!-- Nav Item - Charts -->
   <li class="nav-item">
     <a class="nav-link" href="?mod=user">
       <i class="fas fa-fw fa-user"></i>
       <span>Quản lý Tài khoản</span></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="?mod=faculty">
       <i class="fas fa-fw fa-guitar"></i>
       <span>Quản lý Khoa</span></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="?mod=major">
       <i class="fas fa-fw fa-book"></i>
       <span>Quản lý Ngành</span></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="?mod=class">
       <i class="fas fa-fw fa-bell"></i>
       <span>Quản lý Lớp</span></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="?mod=course">
       <i class="fas fa-fw fa-book-reader"></i>
       <span>Quản lý Học Phần</span></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="?mod=semester">
       <i class="fas fa-fw fa-clock"></i>
       <span>Quản lý Học Kỳ</span></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="?mod=class_section">
       <i class="fas fa-fw fa-table"></i>
       <span>Quản lý Lớp Học Phần</span></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="?mod=regis_course">
       <i class="fas fa-fw fa-table"></i>
       <span>Quản lý Sinh Viên Đăng Ký Học Phần</span></a>
   </li>
   <!-- Sidebar Toggler (Sidebar) -->
   <div class="text-center d-none d-md-inline">
     <button class="rounded-circle border-0" id="sidebarToggle"></button>
   </div>

 </ul>
 <!-- End of Sidebar -->