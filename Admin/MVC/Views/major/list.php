<a href="?mod=major&act=add" type="button" class="btn btn-primary">Thêm mới</a>
<?php if (isset($_COOKIE['msg'])) { ?>
  <div class="alert alert-success">
    <strong>Thông báo</strong> <?= $_COOKIE['msg'] ?>
  </div>
<?php } ?>
<hr>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th scope="col">STT</th>
      <th scope="col">Mã Ngành</th>
      <th scope="col">Tên Ngành</th>
      <th scope="col">Tên Khoa</th>
      <th>#</th>
    </tr>
  </thead>
  <?php $i = 1; ?>
  <tbody>
    <?php foreach ($data as $row) { ?>
      <tr>
        <td><?= $i++ ?></td>
        <th scope="row"><?= $row['id'] ?></th>
        <td><?= $row['name'] ?></td>
        <td><?= $row['faculty_name'] ?></td>
        <td>
          <a href="?mod=major&act=detail&id=<?= $row['id'] ?>" type="button" class="btn btn-success">Xem</a>
          <a href="?mod=major&act=edit&id=<?= $row['id'] ?>" type="button" class="btn btn-warning">Sửa</a>
          <a href="?mod=major&act=delete&id=<?= $row['id'] ?>" onclick="return confirm('Bạn có thật sự muốn xóa ?');" type="button" class="btn btn-danger">Xóa</a>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(document).ready(function() {
    $('#dataTable').DataTable();
  });
</script>